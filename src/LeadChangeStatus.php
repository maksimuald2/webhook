<?php

namespace App;

class LeadChangeStatus extends LeadChanges
{
    public function __construct(array $hook)
    {
        parent::__construct($hook);
        $this->response = $this->response['status'][0];
    }
}
