<?php

namespace App;

abstract class LeadChanges
{
    protected $response;

    public function __construct(array $hook)
    {
        $this->response = $hook['leads'];
    }
    public function getResponse()
    {
        return $this->response;
    }
    public function getLeadID()
    {
        return $this->response['id'];
    }
    public function getName()
    {
        return $this->response['name'];
    }
    public function getPrice()
    {
        return $this->response['price'];
    }
    public function getResponsibleID()
    {
        return $this->response['responsible_user_id'];
    }
    public function getLastModif()
    {
        return $this->response['last_modified'];
    }
    public function getModifUserID()
    {
        return $this->response['modified_user_id'];
    }
    public function getCreatedUserID()
    {
        return $this->response['created_user_id'];
    }
    public function getDateCreate()
    {
        return $this->response['date_create'];
    }
    public function getPipelineID()
    {
        return $this->response['pipeline_id'];
    }
    public function getAccountID()
    {
        return $this->response['account_id'];
    }
    public function getCreatedAt()
    {
        return $this->response['created_at'];
    }
    public function getUpdatedAt()
    {
        return $this->response['updated_at'];
    }
    public function getOldStatusID()
    {
        return $this->response['old_status_id'];
    }    
    public function getCFs()
    {
        return $this->response['custom_fields'];
    }
}
