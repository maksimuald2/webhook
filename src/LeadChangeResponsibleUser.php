<?php

namespace App;

class LeadChangeResponsibleUser extends LeadChanges
{
    public function __construct(array $hook)
    {
        parent::__construct($hook);
        $this->response = $this->response['responsible'][0];
    }
    public function getOldResponsibleUserID()
    {
        return $this->response['old_responsible_user_id'];
    }
}