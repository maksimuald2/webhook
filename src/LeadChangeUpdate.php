<?php

namespace App;

class LeadChangeUpdate extends LeadChanges
{
    public function __construct(array $hook)
    {
        parent::__construct($hook);
        $this->response = $this->response['update'][0];
    }
}
