<?php
require(__DIR__ . '/vendor/autoload.php');

use App\LeadChangeResponsibleUser;
use App\LeadChangeStatus;
use App\LeadChangeUpdate;

$status = json_decode(file_get_contents(__DIR__ . '/status.json'), true);
$responsible = json_decode(file_get_contents(__DIR__ . '/responsible.json'), true);
$update = json_decode(file_get_contents(__DIR__ . '/update.json'), true);

$leadChangeStatus = new LeadChangeStatus($status);
$leadChangeResponsible = new LeadChangeResponsibleUser($responsible);
$leadChangeUpdate = new LeadChangeUpdate($update);

print(
    'LEAD CHANGE STATUS' . PHP_EOL .
    'id: ' . $leadChangeStatus->getLeadID() . PHP_EOL .
    'name: ' . $leadChangeStatus->getName() . PHP_EOL .
    'old status: ' . $leadChangeStatus->getOldStatusID() . PHP_EOL .
    'status id: ' . $leadChangeStatus->getName() . PHP_EOL .
    'price: ' . $leadChangeStatus->getPrice() . PHP_EOL .
    'responsible user id: ' . $leadChangeStatus->getResponsibleID() . PHP_EOL .
    'last modif: ' . $leadChangeStatus->getLastModif() . PHP_EOL .
    'modif user id: ' . $leadChangeStatus->getModifUserID() . PHP_EOL .
    'created user id: ' . $leadChangeStatus->getCreatedUserID() . PHP_EOL .
    'date create: ' . $leadChangeStatus->getDateCreate() . PHP_EOL .
    'account id: ' . $leadChangeStatus->getAccountID() . PHP_EOL .

    'LEAD CHANGE RESPONSIBLE' . PHP_EOL .
    'id: ' . $leadChangeResponsible->getLeadID() . PHP_EOL .
    'name: ' . $leadChangeResponsible->getName() . PHP_EOL .
    'old responsible user id: ' . $leadChangeResponsible->getOldResponsibleUserID() . PHP_EOL .
    'status id: ' . $leadChangeResponsible->getName() . PHP_EOL .
    'price: ' . $leadChangeResponsible->getPrice() . PHP_EOL .
    'responsible user id: ' . $leadChangeResponsible->getResponsibleID() . PHP_EOL .
    'last modif: ' . $leadChangeResponsible->getLastModif() . PHP_EOL .
    'modif user id: ' . $leadChangeResponsible->getModifUserID() . PHP_EOL .
    'created user id: ' . $leadChangeResponsible->getCreatedUserID() . PHP_EOL .
    'date create: ' . $leadChangeResponsible->getDateCreate() . PHP_EOL .
    'account id: ' . $leadChangeResponsible->getAccountID() . PHP_EOL .

    'LEAD CHANGE UPDATE' . PHP_EOL .
    'id: ' . $leadChangeUpdate->getLeadID() . PHP_EOL .
    'name: ' . $leadChangeUpdate->getName() . PHP_EOL .
    'old status id: ' . $leadChangeUpdate->getOldStatusID() . PHP_EOL .
    'status id: ' . $leadChangeUpdate->getName() . PHP_EOL .
    'price: ' . $leadChangeUpdate->getPrice() . PHP_EOL .
    'responsible user id: ' . $leadChangeUpdate->getResponsibleID() . PHP_EOL .
    'last modif: ' . $leadChangeUpdate->getLastModif() . PHP_EOL .
    'modif user id: ' . $leadChangeUpdate->getModifUserID() . PHP_EOL .
    'created user id: ' . $leadChangeUpdate->getCreatedUserID() . PHP_EOL .
    'date create: ' . $leadChangeUpdate->getDateCreate() . PHP_EOL .
    'account id: ' . $leadChangeUpdate->getAccountID() . PHP_EOL
);

echo PHP_EOL;
var_dump($leadChangeUpdate->getCFs());
